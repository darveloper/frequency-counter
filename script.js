

document.getElementById("countButton").onclick = function getFrequency() {
    // your code will go here ...

    let typedText = document.getElementById("textInput").value;
    typedText = typedText.toLowerCase();
    typedText = typedText.replace(/[^a-z'\s]+/g, "");
    const letterCounts = {};
    
    const words = typedText.split(" ");
    for (let i = 0; i < typedText.length; i++) {
        let currentLetter = typedText[i];
        if(letterCounts[currentLetter] === undefined) {
            letterCounts[currentLetter] = 1;
         } else {
            letterCounts[currentLetter]++;
         }
        // do something for each letter.


     };

     for ( currentLetter in letterCounts) {
        const span = document.createElement("span");
        const textContent = document.createTextNode('"' + currentLetter + ": " + letterCounts[currentLetter] + '"' + ', ');
        span.appendChild(textContent);
        document.getElementById("lettersDiv").appendChild(span);
     }
     const wordCount = {};
    
     for (let j = 0; j < words.length; j++) {
        let currentWord = words[j]
        // wordCount[currentWord] = wordCount[currentWord] || 0;
        // wordCount[currentWord]++;
        if(wordCount[currentWord] === undefined)  {
            wordCount[currentWord] = 1;
        }
        else {
            wordCount[currentWord]++
        }

    

    }
    for (currentWord in wordCount) {
        const span = document.createElement("span");
        const textContent = document.createTextNode('"' + currentWord + ": " + wordCount[currentWord] + '"' + ', ');
        span.appendChild(textContent);
        document.getElementById("wordsDiv").appendChild(span);
    }

};